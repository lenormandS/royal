module.exports = function(grunt){

    require('load-grunt-tasks')(grunt)
    grunt.initConfig({
      jshint:{
        all:['public/js/**/*.js','!public/js/app-min.js']
      },
      concat: {
        options : ';\n',
        dist:{
          src:['public/js/app.js','public/js/service/*.js','public/js/controller/*.js'],
          dest:'public/js/app-min.js',
        }
      },
      uglify: {
        options : {
          mangle:false
        },
        my_target: {
          files: {
            'public/js/app-min.js': ['public/js/app-min.js']
          }
        }
      },
      cssmin: {
        options: {
          mergeIntoShorthands: false,
          roundingPrecision: -1
        },
        target: {
          files: {
            'public/css/main-min.css': ['public/css/animate.css', 'public/css/main.css']
          }
        }
      },
      watch :{
        js:{
          files: ['public/js/**/*.js','!public/js/*-min.js'],
          tasks: ['jshint','concat','uglify'],
          options: {
            spawn : false
          }
        },
        css:{
          files: ['public/css/*.css','!public/css/*-min.css'],
          tasks: ['cssmin','concat','uglify'],
          options: {
            spawn : false
          }
        },
      },
    });


    grunt.registerTask('default', ['jshint','concat','uglify','cssmin']);
}
