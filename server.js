var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var player = require('./module/playerServer');
var tchat = require('./module/tchatServer');
var mail = require('./module/mailServer');

function addToken() {
    var newToken = Math.floor((Math.random() * 300) + 1)
    return newToken;
}

app.use(express.static(__dirname + '/public'));
app.get('/', function (req, res, next) {
    res.sendFile(__dirname + '/index.html');
});

server.listen(1337);

io.on('connection', function (client) {

    player.setIo(io);

    player.setClient(client);

    player.newClient(client.id);

    player.addGuest(client.id);

    client.on('monToken', function (data) {
        player.addPlayer(data.token);
    });

    tchat.LastMessage();

    client.on('newMessage', function (data) {
        tchat.Message(data);
    });

    client.on("mailSender",function(data){
        mail.traitementMail(data)
    })

    client.on('myMail',function(data){
        var check = mail.checkMail(data)
        client.emit("mail",check)
    })

    client.on('disconnect', function () {
        player.disconnect(client.id);
    });

});
