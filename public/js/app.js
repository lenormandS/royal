var app = angular.module('App',['ngRoute','ngAnimate']);
app.config(function($routeProvider){
    $routeProvider
            .when('/',{templateUrl:'view/home.html'})
            .when('/mail',{templateUrl:'view/mail.html',controller:"mailCtrl"})
            .when('/mail/:id',{templateUrl:'view/mail.html',controller:"mailCtrl"})
            .when('/rule',{templateUrl:'view/rule.html'})
            .otherwise({redirectTo:'/'});
});
