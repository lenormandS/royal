app.factory('ServiceFactory', function (socket) {
    var factory = {
        id: "",
        token: null,
        guests: [],
        sender:"",
        createToken: function () {
            var token = Math.floor((Math.random() * 300) + 1);
            factory.setToken(token);
            socket.emit('monToken', {
                token: factory.token
            });
        },
        setId: function (id) {
            factory.id = id;
        },
        getId: function () {
            return factory.id;
        },
        setToken: function (token) {
            factory.token = token;
        },
        getToken: function () {
            if (factory.token === null) {
                factory.createToken();
                return factory.token;
            } else {
                return factory.token;
            }
        },
        getGuests: function () {
            return factory.guests;
        },
        setGuests: function (guests) {
            factory.guests = guests;
        },
        getSender: function(){
            return factory.sender;
        },
        setSender: function(sender){
            factory.sender = sender;
        }
    };

    return factory;
});
