app.controller("mailCtrl", function ($scope, $routeParams, ServiceFactory, socket) {
    $scope.param = $routeParams.id;
    $scope.me = ServiceFactory.getToken();
    $scope.closeModal = function () {
        $("#MailModal").modal("hide");
    };
    $scope.addHTML = function(type){
      var addingHTMl = '['+type+'][/'+type+']';
      document.getElementById('mailtexte').innerHTML = addingHTMl;
    };
    $scope.integrationHTML = function(text){
      console.log(text);
      return text;
    };
    $scope.messageSender = function () {
        var title = $scope.title;
        var message = $scope.textearea;
        var receveur = ServiceFactory.getSender();
        $("#MailModal").modal("hide");
        socket.emit('mailSender', {
            title: title,
            message: message,
            emeteur: ServiceFactory.getToken(),
            receveur: receveur
        });
    };
    $scope.MailConservation = function(tab){
      $scope.mail = tab[0];
    };
    $scope.verifMail = function () {
        socket.emit("myMail", $scope.me);
    };
    socket.on('mail',function(data){
        $scope.message = data;
    });
});
