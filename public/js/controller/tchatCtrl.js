app.controller("tchatCtrl", function ($scope, socket, ServiceFactory) {

    $scope.je = ServiceFactory.getToken();

    $scope.idPlaceTchat = '#tchattext';

    $scope.formTchat = function (nodetype, classList, textnode,typeanime) {
        var node = document.createElement(nodetype);
        node.classList.add(classList,'animated', typeanime);
        var text = document.createTextNode(textnode);
        node.append(text);
        $($scope.idPlaceTchat).append(node);
    };

    $scope.datainput = function (user, content) {
        var datainputtext = "Guest" + user + " dit : " + content;
        return datainputtext;
    };

    $scope.tchatinput = function () {
        var input = $("#inputtchat").val();
        if (input != "") {
            $scope.formTchat('DIV', 'mestext', input,"lightSpeedIn");
            socket.emit('newMessage', {
                guest: ServiceFactory.getToken(),
                message: input
            });
        } else {
            $scope.formTchat('DIV', 'errormessage', "Vous n'avez rien ecris");
        }
        $("#inputtchat").val('');
    };

    socket.on('lastMessage', function (data) {
        for (var i = 0; i < data.length; i++) {
            $scope.formTchat('DIV', 'othertext', $scope.datainput(data[i].guest, data[i].message));
        }
    });

    socket.on('messageReceveur', function (data) {
        var obj = data[(data.length - 1)];
        if (obj.guest != $scope.je) {
            $scope.formTchat('DIV', 'othertext', $scope.datainput(obj.guest, obj.message),"fadeInLeft");
        }
    });

    $scope.toggleTchat = function () {
        $("#tchatbody").toggle("slow");
    };
});
