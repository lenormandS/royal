app.controller('guestCtrl', function ($scope, socket, ServiceFactory) {
    $scope.me = ServiceFactory.getToken();

    $scope.id = ServiceFactory.getId();

    $scope.guests = ServiceFactory.getGuests();

    $scope.sendInvite = function (param) {
        ServiceFactory.setSender(param);
        $('#MailModal').modal({backdrop: "static"});
    };

    socket.on('guestPlayer', function (data) {
        ServiceFactory.setGuests(data);
        $scope.guests = ServiceFactory.getGuests();
    });

    socket.on('addclient', function (data) {
        ServiceFactory.setId(data);
    });

    socket.emit('disconnect', function (data) {
        data = $scope.me;
    });

});
