var player = require('./playerServer');

/*
 * Type de module : Optionnel,
 * Herite : PlayerServer
 * Module de recuperation uniquement pour le Tchat,
 * utilise le module PlayerServer afin de recupéré les données des joueurs en Guest
 */

var tchatServer = {
    messageArray: [],
    maxMessage: 99,
    minMessage: 10,

    Message: function (param) {
        this.messageArray.push({guest: param.guest, message: param.message});
        if (this.messageArray.length > this.maxMessage) {
            this.messageArray.shift();
        }
        player.getIo().emit('messageReceveur', this.messageArray);
    },
    LastMessage : function (){
        var history = [];
        if(this.messageArray.length != 0){
            var reverse = this.messageArray.reverse();
            if(this.messageArray.length <= this.minMessage){
                for(var i = 0 ; i < reverse.length; i++){
                    history.push(reverse[i]);
                }
            }
            player.getClient().emit("lastMessage",history)
        }
    }
}

module.exports = tchatServer
