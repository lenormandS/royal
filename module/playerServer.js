/* 
 * Type de module : Important,
 * Fille : tchatServer,mailServer
 *  
 * utilise le module PlayerServer afin de recupéré les données des joueurs en Guest
 */

var playerServer = {
    Io : {},
    Client: {},
    Guest:[],
    Player : [],
   getIo : function(){
        return playerServer.Io;
    },
    setIo : function(io){
      playerServer.Io = io;  
    },
    getClient: function () {
        return playerServer.Client;
    },
    setClient: function (client) {
        playerServer.Client = client;
    },
    addGuest : function(param){
        playerServer.Guest.push(param);
    },
    disconnect : function(param){
        var num = playerServer.Guest.indexOf(param);
        playerServer.Guest.splice(num, 1);
        playerServer.Player.splice(num, 1);
        playerServer.getIo().emit('guestPlayer', playerServer.Player);
    },
    addPlayer : function(param){
        playerServer.Player.push(param);
        playerServer.getIo().emit('guestPlayer', playerServer.Player);
    },
    newClient: function (param) {
        playerServer.getClient().emit('addclient', param);
    }
}

module.exports = playerServer