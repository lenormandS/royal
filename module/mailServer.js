var player = require("./playerServer");

/*
 * Type de module : Optionnel,
 * Herite : PlayerServer
 * Module de recuperation uniquement pour les messages,
 * utilise le module PlayerServer afin de recupéré les données des joueurs en Guest
 */

var mailServer = {
    Mail: [],
    traitementMail: function (param) {
        this.Mail.push(param)
    },
    checkMail : function (id){
        var mesMail = [];
        if(this.Mail.length != 0){
            for(var i = 0 ; i < this.Mail.length; i++){
                if(this.Mail[i].receveur === id){
                    mesMail.push(this.Mail[i]);
                }
            }
            return mesMail;
        }else{
            return 0;
        }
    }
}

module.exports = mailServer
